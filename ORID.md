### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	Today we had the usual StandMeeting and Code Review. In the morning, we learned Base sql, then introduced springboot jpa by analyzing sql, and practiced Mysql demo. In the afternoon, we worked on yesterday's case study and extensively refactored the system to link it to a real database. It's also worth noting that for test modifications, the original mock Object data should be replaced with the real returned data. Today's lesson was generally less difficult, but it was also paving the way for what was to come.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Bustling.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	The most meaningful part of today's lesson is connecting the test data to a real database, as opposed to the fake data we've been mocking, which is more reflective of real development operations.

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	From the writing of tests, to the development of the business, to the introduction of the database and the modification of the tests, I was able to clearly perceive the entire development process, which also has a reference meaning for the actual business development in the future.